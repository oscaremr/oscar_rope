/*jslint white: true, browser: true*/
/*global jQuery, JoelPurra, debug*/

(function($) {
    "use strict"; // jshint ;_;
    (function(namespace) {

        // Calling https://gist.github.com/2254354
        namespace.autoCleanNorthAmericanPhoneNumber = function($input) {

            debug.log(".autoCleanNorthAmericanPhoneNumber($input)", $input);

            // From http://blog.stevenlevithan.com/archives/validate-phone-number
            var phoneNumberUnformatted = /^\(?([0-9]{3})\)?[\-. ]?([0-9]{3})[\-. ]?([0-9]{4})$/,
                replaceWithFormatted = "($1) $2-$3";

            namespace.autoCleanReplace($input, phoneNumberUnformatted, replaceWithFormatted);
        };
    }(JoelPurra));

    (function() {

        // TODO: patch JoelPurra.autoClean() to accept multiple elements
        var $telephoneInputs = $("[type=tel]");

        $telephoneInputs.each(function() {
            JoelPurra.autoCleanNorthAmericanPhoneNumber($(this));
        });

    }());

    (function(namespace) {

        // Calling https://gist.github.com/2254354
        namespace.autoCleanCanadianPostalCode = function($input) {

            debug.log(".autoCleanCanadianPostalCode($input)", $input);

            var postalCodeUnformatted = /^(\w\d\w)\s*(\d\w\d)$/i,
                replaceWithFormatted = "$1 $2";

            namespace.autoCleanReplace($input, postalCodeUnformatted, replaceWithFormatted);
            namespace.autoCleanUpperCase($input);
        };
    }(JoelPurra));

    (function(namespace) {

        // Calling https://gist.github.com/2254354
        namespace.autoCleanUpperCase = function($input) {

            debug.log(".autoCleanUpperCase($input)", $input);

            function cleanUpperCase($inputToClean) {

                debug.log(".autoCleanUpperCase($input)", $input, "cleanUpperCase($inputToClean)", $inputToClean);

                var val = $inputToClean.val(),
                    clean = val.toUpperCase();

                if (val !== clean) {

                    return clean;
                }

                return null;
            }

            namespace.autoCleanCallback($input, cleanUpperCase);
        };
    }(JoelPurra));

    (function() {

        // A fragile way of matching postal code inputs, but works locally
        var $postalCodeInputs = $("[placeholder=A\\#A\\ \\#A\\#]");

        $postalCodeInputs.each(function() {
            JoelPurra.autoCleanCanadianPostalCode($(this));
        });

    }());
}(jQuery));