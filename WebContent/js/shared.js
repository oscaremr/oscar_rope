// Wait list forms shared code

/*jslint vars: true, white: true, browser: true*/
/*global jQuery, debug, console*/

(function($, window, debug) {
    "use strict";

    (function() {

        if (debug.setLevel) {
            debug.setLevel(2);
        }
    }());

    var $window = $(window),
        $body = $(document.body),
        togglerSelector = '[data-toggle="tab"], [data-toggle="pill"]',
        tabsSelector = ".tab-content .tab-pane";

    $.fn.extend({
        filterLinksToFragment: function(fragment) {
            var $this = $(this),
                $filtered = $this.filter("a:urlFragment").filter(function() {
                    var $this = $(this),
                        thisFragment = $.param.fragment($this.attr('href'));

                    return (thisFragment === fragment);
                });

            return $filtered;
        }
    });

    function showFragmentTab(fragment) {
        $(togglerSelector).filterLinksToFragment(fragment).tab("show");
    }

    function getTabContentElement($anchor) {
        return $("#" + $.param.fragment($anchor.attr('href')));
    }

    // TODO: when using tabs for navigation, apply tabs here with
    // javascript. Otherwise the css .tab-content .tab-pane will hide
    // everything for non-js browsers.

    (function() {
        // Focus the first field after selecting a tab
        $body.on('shown', togglerSelector, function(e) {
            var $this = $(this),
                $content = getTabContentElement($this);

            $content.find(":input:visible").first().focus();
        });
    }());

    (function() {
        function showAllTabsForValidation() {
            var $tabs = $(tabsSelector);

            $tabs.addClass(activeClass);
        }

        function hideAllTabsExceptOne($except) {
            var $tabs = $(tabsSelector),
                id = $except.attr("id");

            $tabs.not($except).removeClass(activeClass);

            showFragmentTab(id);
        }

        function getFocused() {
            return $(document.activeElement);
        }

        function confirmSubmission() {
            var submissionConfirmationQuestion = "Only submit the form when you have completed the entirety of the form." + "\n\n" + "Please confirm that you want to submit the application form.",
                submitConfirmed = window.confirm(submissionConfirmationQuestion);

            return submitConfirmed;
        }

        function hideAllTabsExceptFocusedAndScrollThere() {
            var $focused = getFocused(),
                $parentTab = $focused.parents(activeSelector)
                // Fallback
                .add($(tabsSelector).last());

            hideAllTabsExceptOne($parentTab.first());

            if ($focused.length === 1) {
                $focused.scrollToCenterScreen();
            }
        }

        function formDoneCallback() {
            // TODO: make me pretty and usable
            window.alert("Successfully submitted the form.\n\nThank you for your application!");
        }

        function formFailCallback() {
            // TODO: make me pretty and usable
            window.alert("An error occurred saving the form.\n\nYou can try to submit it again, or contact us by phone if the problem persists. We are sorry for the inconvenience.");
        }

        var $forms = $('form'),
            activeClass = "active",
            activeSelector = tabsSelector + "." + activeClass;

        $.fn.extend({
            scrollToCenterScreen: function() {
                $('html, body').animate({
                    scrollTop: Math.max(0, $(this).offset().top - ($window.height() / 2))
                });
            }
        });

        $forms.h5Validate();

        $forms.on("formValidate", function(evt, data) {
            showAllTabsForValidation();
        });

        $forms.on("formValidated", function(evt, data) {});

        // Might be able to make use of a new submitValidate event in h5Validate?
        // h5Validate does focus managment at this point, and parts of this code
        // relies on focus being set.
        $forms.on("submit", function(evt, data) {
            var result = false,
                ajaxSubmitPromise = null;

            // Since all tabs are showing, all fields will be submitted unless hiding them
            if (!evt.isDefaultPrevented()) {
                if (!confirmSubmission()) {
                    evt.preventDefault();
                } else {
                    // HACK: don't submit the form, but pass all values to a custom
                    // "backend" that will take care of submitting translating the
                    // current form to a format that suits the "real backend", eForms.
                    // This will be done with an ajax post, so have to cancel this submit.
                    ajaxSubmitPromise = Backend.submitHandler(evt, data).done(formDoneCallback).fail(formFailCallback);
                }
            }

            if (evt.isDefaultPrevented() || result === false) {
                // Back to normal tab mode
                hideAllTabsExceptFocusedAndScrollThere();
            }

            return result;
        });
    }());

    (function() {
        // Handle tabs, fragments and pushState

        function pushStateFromHref(ahref) {

            var $ahref = $(ahref),
                fragment;

            if (!$ahref.is("a:urlFragment")) {
                return;
            }

            fragment = $.param.fragment($ahref.attr('href'));

            $.bbq.pushState(fragment, 2);
        }

        // Workaround for Bootstrap Tabs' e.preventDefault()
        $body.on('shown', togglerSelector, function(e) {

            pushStateFromHref(e.target);
        });

        function showTheCurrentFragmentTab() {
            var currentFragment = $.param.fragment();

            if (currentFragment !== undefined && currentFragment !== "") {
                showFragmentTab(currentFragment);
            }
        }

        $window.on('hashchange', function() {
            // Activate the current URL's #part-anything tab
            showTheCurrentFragmentTab();
        });

        // Trigger the event (useful on page load).
        $window.hashchange();
    }());

    (function() {
        // Inject next/previous buttons for form parts
        var eventNamespace = ".nextPreviousFormPartButtons";

        function getCurrent($elementInside) {
            var $tab = $elementInside.closest(tabsSelector);

            return $tab;
        }

        function getPrevious($elementInside) {
            return getCurrent($elementInside).prev(tabsSelector);
        }

        function getNext($elementInside) {
            return getCurrent($elementInside).next(tabsSelector);
        }

        function showPrevious(evt) {
            var $this = $(evt.target),
                $previous = getPrevious($this);

            if ($previous.length === 1) {
                showFragmentTab($previous.attr("id"));
            }

            evt.preventDefault();
            evt.stopImmediatePropagation();
            return false;
        }

        function showNext(evt) {
            var $this = $(evt.target),
                $next = getNext($this);

            if ($next.length === 1) {
                showFragmentTab($next.attr("id"));
            }

            evt.preventDefault();
            evt.stopImmediatePropagation();
            return false;
        }

        function getTabNextPreviousNavigationBlock() {
            var $container = $("<div />").addClass("tab-next-previous-navigation form-actions"),
                // <button>s are type="button" by default, adding it to be explicit about using them for this script only
                $next = $("<button />").text("Go to the next form part").attr("type", "button").addClass("next btn-primary").on("click" + eventNamespace, showNext),
                $previous = $("<button />").text("Go to the previous form part").attr("type", "button").addClass("previous btn-mini").on("click" + eventNamespace, showPrevious);

            $container.append($next).append($previous);

            return $container;
        }

        $(function() {
            var $tabs = $(tabsSelector);

            $tabs.each(function(index) {
                var $this = $(this),
                    $block;

                if (index === ($tabs.length - 1)) {
                    // Last .tab-pane is assumed to have submit buttons
                    return;
                }

                $block = getTabNextPreviousNavigationBlock();

                if (index === 0) {
                    // Disable previous button for the first .tab-pane
                    $block.find(".previous").addClass("disabled").attr("disabled", "disabled");
                }

                $this.append($block);
            });
        });
    }());

    (function() {
        function repeat(filler, length) {
            return (new Array(length + 1)).join(filler);
        }

        function fill(length, filler, text) {
            var result = (String() + repeat(filler, length) + text);

            return result.substring(result.length - length);
        }

        function formatDate(date, format) {
            return format.replace("yyyy", date.getFullYear()).replace("MM", fill(2, "0", date.getMonth() + 1)).replace("dd", fill(2, "0", date.getDate()));
        }

        $(function() {
            var now = new Date(),
                // TODO: pass the date in a cleaner way, without global variables
                dateFormat = window.dateFormat || "yyyy-MM-dd",
                dateFormatted = formatDate(now, dateFormat),
                yearFormatted = formatDate(now, "yyyy");

            $(".set-text-date").text(dateFormatted);
            $(".set-max-current-year").attr("max", yearFormatted);
        });
    }());

}(jQuery, window, debug || console));