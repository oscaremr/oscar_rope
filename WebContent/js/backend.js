(function($, window, debug) {
    "use strict";
    window.Backend = {};

    var ns = window.Backend,
        priv = {};

    ns.submitHandler = function(evt, data) {
        var tag = "ns.submitHandler",
            $form = $(evt.target);

        evt.preventDefault();

        return ns.submit($form);
    };

    ns.submit = function($form) {
        function submitDone(data, textStatus, jqXHR) {
            var tag = "ns.submit.submitDone";

            debug.log(tag, arguments);

            submitDeferred.resolveWith($form, [data, textStatus, jqXHR]);
        };

        function submitFail(jqXHR, textStatus, errorThrown) {
            var tag = "ns.submit.submitFail";

            debug.error(tag, arguments);

            submitDeferred.rejectWith($form, [jqXHR, textStatus, errorThrown]);
        }

        var tag = "ns.submit",
            fields = priv.getSuccessfulFields($form),
            url = $form.data("eform-action") || $form.attr("action") || "",
            formSpecificMapping = window[$form.data("eform-mapping")],
            mappedFields = null,
            submitDeferred = new $.Deferred(),
            ajaxPromise = null;

        if ($form.length !== 1) {
            var msg = "Wasn't called with a single form.";

            debug.error(tag, msg, $form);

            throw new Error(msg);
        }

        if (!$.isFunction(formSpecificMapping)) {
            var msg = "Could not find a form-specific mapping.";

            debug.error(tag, msg, formSpecificMapping);

            throw new Error(msg);
        }

        mappedFields = priv.mapFields(formSpecificMapping, fields);

        debug.log(tag, "fields", fields, "mappedFields", mappedFields);

        ajaxPromise = $.post(url, mappedFields).done(submitDone).fail(submitFail);

        return submitDeferred.promise();
    };

    priv.mapFields = function(formSpecificMapping, fields) {
        var tag = "priv.mapFields",
            filteredFields = priv.removeEmptyFields(fields);

        $.each(filteredFields, function(index, field) {
            field.handled = false;
        });

        var mappedFields = formSpecificMapping(filteredFields, []);

        var fallbackMapped = priv.fallbackMapping(filteredFields, mappedFields);

        var allMapped = mappedFields.concat(fallbackMapped);

        var ajaxData = priv.fieldsToAjaxData(allMapped);

        return ajaxData;
    };

    priv.fallbackMapping = function(fields, mappedFields) {
        var tag = "priv.fallbackMapping",
            mapped = [];

        $.each(fields, function(index, field) {
            if (field.handled !== true) {
                debug.log(tag, "Could not find mapping for field, using default passthrough mapping.", field.name, field);

                field.handled = true;
                mapped.push(field);
            }
        });

        return mapped;
    };

    priv.fieldsToAjaxData = function(fields) {
        var ajaxData = [];

        $.each(fields, function(index, field) {
            if (!field) {
                return;
            }

            var nameValuePair = {
                name: field.name,
                value: field.value
            };

            ajaxData.push(nameValuePair);
        });

        return ajaxData;
    };

    priv.removeEmptyFields = function(fields) {
        var filteredFields = $.grep(fields, function(field) {
            if (field.name === "" || field.normalizedName === "" || field.value === "") {
                return false;
            }

            return true;
        });

        return filteredFields;
    };

    priv.getSuccessfulFields = function($form) {
        // According to http://www.w3.org/TR/html401/interact/forms.html#h-17.13.2
        // fields that are hidden with CSS may still be successful
        var $inputs = $form.find(":input").not(":disabled").not(":hidden"),
            fields = [];

        $inputs.each(function(index, input) {
            var $input = $(input),
                name = input.name,
                field = {
                    name: name,
                    normalizedName: priv.normalizeName(name),
                    value: $input.val()
                };

            if (($input.is(":checkbox") || $input.is(":radio")) && $input.prop("checked") !== true) {
                return;
            }

            fields.push(field);
        });

        return fields;
    };

    priv.normalizeName = function(name) {
        var indexerRx = /\[\d+\]/g,
            normalizedName = name.replace(indexerRx, "[i]");

        return normalizedName;
    };

}(jQuery, window, debug || console));