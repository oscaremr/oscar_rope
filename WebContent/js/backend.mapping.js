(function(global, debug) {
    "use strict";
    // Regexp quick search-replace fix for the exported field name/values lists
    // ^"([^"]+)" (.*)$
    // "\1": {\n"mode": "translate",\n"htmlName": "\1"\n,"eFormName": "\1",\n"htmlValues": [\2],\n"eFormValues": [\2],\n"custom": function(fields, mappedFields, field){\nvar copy = priv.copyField(field);\n\nreturn copy;}\n},
    //
    // var someMappings = {
    //     "my-mapping-field-name": {
    //         "mode": "translate handled",
    //         "htmlName": "my-mapping-field-name",
    //         "eFormName": "My Mapping Field Name",
    //         "htmlValues": ["value1", "value2"],
    //         "eFormValues": ["outputvalue1", "outputvalue2"],
    //         "custom": function(fields, mappedFields, field) {
    //             // Only used for custom mappers
    //             var copy = priv.copyField(field);
        
    //             return copy;
    //         }
    //     },
    //     ...
    // };
    var priv = {},
        ns = null;

    window.Backend.Mapping = ns = window.Backend.Mapping || {};

    priv.map = function(fields, mappedFields, mapper, matchingFields) {
        var tag = "priv.map",
            knownMapperModes = ["translate", "translateStrict", "customField", "customFieldArray", "passthrough"],
            mapped = [],
            modes = mapper.mode.split(/\s+/g),
            handledFields = [];

        // $.each(modes, function(index, mode) {
        //     debug.log(tag, mapper, matchingFields, "modes", index, mode);
        // });

        //debug.log(tag, mapper, matchingFields);
        $.each(modes, function(index, mode) {
            if (mode === "customFieldArray") {
                // Use the mapper as context
                var result = mapper.custom.call(mapper, fields, mappedFields, matchingFields);

                mapped = mapped.concat(result);
            } else {
                $.each(matchingFields, function(index, field) {
                    if ($.inArray(mode, knownMapperModes) !== -1 && field.handled !== false) {
                        debug.warn(tag, "Field was already handled, skipping.", mapper, index, matchingFields, field);

                        return;
                    }

                    // debug.log(tag, mapper, matchingFields, "matchingFields", index, field);

                    if (mode === "passthrough") {
                        var translated = priv.copyField(field);

                        mapped.push(translated);
                    } else if (mode === "translate") {
                        var translated = priv.translateHtmlToEFormLoose(mapper, field);

                        mapped.push(translated);
                    } else if (mode === "translateStrict") {
                        var translated = ns.translateHtmlToEFormStrict(mapper, field);

                        if (translated.value !== undefined) {
                            mapped.push(translated);
                        }
                    } else if (mode === "customField") {
                        // Use the mapper as context
                        var result = mapper.custom.call(mapper, fields, mappedFields, field);

                        mapped = mapped.concat(result);
                    } else if (mode === "handled") {
                        handledFields.push(field);
                    }
                });
            }
        });

        $.each(handledFields, function(index, field) {
            field.handled = true;
        });

        // debug.log(tag, mapper, matchingFields, "mapped", mapped);
        return mapped;
    };

    ns.backendMapping = function(mappings, fields, mappedFields) {
        var tag = "global.sharedBackendMapping",
            mapped = priv.backendMapping(mappings, fields, mappedFields);

        return mapped;
    };

    priv.backendMapping = function(mappings, fields, mappedFields) {
        var tag = "priv.backendMapping",
            mapped = [];

        $.each(mappings, function(index, mapper) {
            var name = mapper.htmlName,
                matchingFields = [],
                mapResults = null;

            // debug.log(tag, "mappings", index, mapper);
            $.each(fields, function(index, field) {
                if (field.name === name) {
                    matchingFields.push(field);
                }
            });

            mapResults = priv.map(fields, mappedFields, mapper, matchingFields);

            // debug.log(tag, "mapResults", mapResults);

            mapped = mapped.concat(mapResults);
        });

        debug.log(tag, "done", mappings, fields, mappedFields, "output", mapped);

        return mapped;
    };

    priv.translateHtmlToEFormLoose = function(mapper, field) {
        var translated = ns.translateHtmlToEFormStrict(mapper, field);

        if (translated.value === undefined) {
            translated.value = field.value;
        }

        return translated;
    };

    ns.translateHtmlToEFormStrict = function(mapper, field) {
        // Translate name
        var translated = ns.createField(mapper.eFormName, undefined);

        // Translate value, if there's a 1:1 array mapping
        if ($.isArray(mapper.htmlValues) && $.isArray(mapper.eFormValues)) {
            var htmlValueIndex = $.inArray(field.value, mapper.htmlValues);

            if (htmlValueIndex !== -1) {
                var eFormValue = mapper.eFormValues[htmlValueIndex];

                if (eFormValue !== undefined) {
                    translated.value = eFormValue;
                }
            }
        }

        return translated;
    };

    ns.createFields = function(name, values) {
        var fields = [];

        if ($.isArray(values)) {
            $.each(values, function(index, value) {
                fields.push(ns.createField(name, value));
            });
        } else {
            // Also allow for a single value to be passed in
            fields.push(ns.createField(name, values));
        }
        return fields;
    };

    ns.createField = function(name, value) {
        var field = {
            name: name,
            value: value
        };

        return field;
    };

    priv.copyField = function(field) {
        var copy = ns.createField(field.name, field.value);

        return copy;
    };

    priv.copy = function(obj) {
        return $.extend(true, {}, obj);
    };
}(window, debug || console));