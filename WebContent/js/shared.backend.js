(function(global, debug) {
    "use strict";
    // ^"([^"]+)" (.*)$
    // "\1": {\n"mode": "translate",\n"htmlName": "\1"\n,"eFormName": "\1",\n"htmlValues": [\2],\n"eFormValues": [\2],\n"custom": function(fields, mappedFields, field){\nvar copy = wbm.copyField(field);\n\nreturn copy;}\n},
    // TODO: move to an external file?
    var sharedMappings = {
        // "": {
        //     "mode": "translate",
        //     "htmlName": "",
        //     "eFormName": "",
        //     "htmlValues": [""],
        //     "eFormValues": [""],
        //     "custom": function(fields, mappedFields, field) {
        //         var copy = wbm.copyField(field);
        //
        //         return copy;
        //     }
        // },
        "gender": {
            // WL Criteria Type fields #13, #14
            // TODO: split into Access1/CASH uniquely named fields (including case)
            "mode": "translate handled",
            "htmlName": "Gender",
            "eFormName": "Gender",
            "htmlValues": ["male", "female", "transgender", "transsexual", "other"],
            "eFormValues": ["male", "female", "transgender", "transsexual", "other"]
        },
        "referral-source": {
            // WL Criteria Type fields #8, #18
            // TODO: there is no input suitable for this translation
            // organizational-referral-source
            // accredited-professional
            // self
            // family-friend
            // hospital
            // ontario-review-board
            // alternative-access-route
            "mode": "translate",
            "htmlName": "referral-source",
            "eFormName": "Referral Source"
        }
    },
        priv = {},
        ns = null,
        wbm = window.Backend.Mapping;

    window.Backend.Shared = ns = window.Backend.Shared || {};

    priv.getAgeGroupFromYears = function(ageYears) {
        var group = undefined;

        if (ageYears >= 14) {
            if (ageYears >= 14 && ageYears <= 22) {
                group = "age-14-22";
            } else if (ageYears >= 16 && ageYears <= 24) {
                group = "age-16-24";
            } else if (ageYears >= 16 && ageYears < 18) {
                group = "age-16-up";
            } else {
                group = "age-18-up";
            }
        }

        return group;
    };

    ns.translateBirthdateToAgeGroup = function(mapper, field, eFormName) {
        var birthDate = new Date(Date.parse(field.value)),
            birthYear = birthDate.getFullYear(),
            ageYears = (new Date()).getFullYear() - birthYear;

        return priv.translateAgeInYearsToAgeGroup(mapper, field, eFormName, ageYears);
    };

    ns.translateAgeInYearsFieldToAgeGroup = function(mapper, field, eFormName) {
        var ageYears = parseInt(field.value, 10);

        return priv.translateAgeInYearsToAgeGroup(mapper, field, eFormName, ageYears);
    };

    priv.translateAgeInYearsToAgeGroup = function(mapper, field, eFormName, ageYears) {
        var group = priv.getAgeGroupFromYears(ageYears);

        if (group !== undefined) {
            var copy = wbm.createField(eFormName, group);

            return copy;
        }

        return undefined;
    };

    ns.Mapping = function(fields, mappedFields) {
        var tag = "ns.SharedMapping",
            mapped = wbm.backendMapping(sharedMappings, fields, mappedFields);

        return mapped;
    };
}(window, debug || console));