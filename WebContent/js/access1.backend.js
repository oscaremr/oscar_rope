(function(global, debug) {
    var access1Mappings = {
        "program-application": {
            // Access1 only
            // WL Criteria Type fields #1, #3, #7
            // TODO: take care of "case-mgmt-first-available"
            "mode": "customField",
            "htmlName": "program-application",
            "custom": function(fields, mappedFields, field) {
                function getProgramAgency(value) {
                    switch (value) {
                    case "case-mgmt-first-available":
                        return undefined;
                    case "across-boundaries":
                    case "across-boundaries-youth":
                        return "across-boundaries";
                    case "bayview-community-services":
                        return "bayview-community-services";
                    case "cmha-toronto-east-case-mgmt":
                    case "cmha-toronto-east-case-mgmt-rehab":
                    case "cmha-toronto-east-actt":
                        return "cmha-toronto-east";
                    case "cmha-toronto-west-case-mgmt":
                    case "cmha-toronto-west-actt":
                        return "cmha-toronto-west";
                    case "cota-health":
                        return "cota-health";
                    case "crct-toronto-hostel":
                    case "crct-toronto-case-mgmt":
                    case "crct-toronto-case-mgmt-tamil-somali":
                    case "crct-toronto-case-mgmt-early":
                        return "community-resource-connections-of-toronto";
                    case "griffin-centre-community-support-network":
                        return "griffin-centre-community-support-network";
                    case "north-york-general-hospital-actt":
                        return "north-york-general-hospital";
                    case "reconnect-mental-health-services-case-mgmt":
                    case "reconnect-mental-health-services-actt":
                        return "reconnect-mental-health-services";
                    case "saint-elizabeth-health-case":
                        return "saint-elizabeth-health-care";
                    case "scarborough-hospital-case-mgmt":
                    case "scarborough-hospital-actt":
                        return "scarborough-hospital";
                    case "sunnybrook-hospital-actt":
                        return "sunnybrook-hospital";
                    case "toronto-north-support-services-case-mgmt":
                    case "toronto-north-support-services-case-mgmt-street":
                    case "toronto-north-support-services-case-mgmt-french":
                        return "toronto-north-support-services";
                    }

                    debug.warn(tag, "getProgramAgency", "Could not find a matching agency", value);

                    return undefined;
                }

                function getProgramArea(value) {
                    switch (value) {
                    case "case-mgmt-first-available":
                        return undefined;
                    case "across-boundaries-youth":
                    case "north-york-general-hospital-actt":
                        return "north-york";
                    case "scarborough-hospital-case-mgmt":
                    case "scarborough-hospital-actt":
                        return "scarborough";
                        return "east-york";
                        return "old-city-of-york";
                        return "north-etobicoke";
                        return "south-etobicoke";
                    case "sunnybrook-hospital-actt":
                        return "downtown-toronto";
                    case "cmha-toronto-east-case-mgmt":
                    case "cmha-toronto-east-case-mgmt-rehab":
                    case "cmha-toronto-east-actt":
                        return "east-of-yonge";
                    case "cmha-toronto-west-case-mgmt":
                    case "cmha-toronto-west-actt":
                        return "west-of-yonge";
                    case "toronto-north-support-services-case-mgmt":
                    case "toronto-north-support-services-case-mgmt-street":
                    case "toronto-north-support-services-case-mgmt-french":
                        return "toronto";
                    case "crct-toronto-case-mgmt":
                    case "crct-toronto-case-mgmt-early":
                        return ["scarborough", "north-york", "downtown-toronto", "west-of-yonge"];
                    case "crct-toronto-case-mgmt-tamil-somali":
                        return ["north-york", "downtown-toronto", "west-of-yonge"];
                    case "saint-elizabeth-health-case":
                        return ["north-york", "north-etobicoke", "south-etobicoke"];

                        // TODO: unmapped below here
                    case "across-boundaries":
                    case "bayview-community-services":
                    case "cota-health":
                    case "crct-toronto-hostel":
                    case "griffin-centre-community-support-network":
                    case "reconnect-mental-health-services-case-mgmt":
                    case "reconnect-mental-health-services-actt":
                        break;
                    }

                    debug.warn(tag, "getProgramArea", "Could not find a matching area", value);

                    return undefined;
                }

                function getProgramType(value) {
                    switch (value) {
                    case "case-mgmt-first-available":
                        return undefined;

                        // TODO: unmapped below here
                    case "long-term-case-management":
                    case "short-term-case-management":
                    case "emergency-department-diversion-program":
                    case "assertive-community-treatment-team":
                    case "mental-health-outreach-program":
                    case "language-specific-service":
                    case "youth-programs":
                    case "early-intervention-programs":
                    case "mental-health-prevention-program-short-term-case-management":
                    case "seniors-case-management":
                    case "tcat-addictions-case-management":
                    case "catch":
                    case "catch-ed":
                        break;
                    }

                    debug.warn(tag, "getProgramType", "Could not find a matching type", value);

                    return undefined;
                }

                var tag = "access1Mappings.program-application",
                    mapped = [],
                    agency = getProgramAgency(field.value),
                    area = getProgramArea(field.value),
                    type = getProgramType(field.value);

                // TODO: suggested improvement: use arrays to return objects with agency/area/type
                // TODO: check mappedFields to avoid duplicates being passed on to the eForm server?
                if (agency) {
                    mapped = mapped.concat(wbm.createFields("Agency", agency));
                }

                if (area) {
                    mapped = mapped.concat(wbm.createFields("Area", area));
                }

                if (type) {
                    mapped = mapped.concat(wbm.createFields("Type of Program", type));
                }

                return mapped;
            }
        },
        "access1-age-years": {
            // WL Criteria Type fields #2 (duplicate mapping "age-years"/"date-of-birth")
            // TODO: use only one of age-years/date-of-birth
            "mode": "customField",
            "htmlName": "age-years",
            "eFormName": "age-years",
            "custom": function(fields, mappedFields, field) {
                var result = [];

                result.push(wbs.translateAgeInYearsFieldToAgeGroup(this, field, "Age"));

                return result;
            }
        },
        "access1-date-of-birth": {
            // WL Criteria Type fields #2 (duplicate mapping "age-years"/"date-of-birth")
            // TODO: use only one of age-years/date-of-birth
            "mode": "customField",
            "htmlName": "date-of-birth",
            "eFormName": "date-of-birth",
            "custom": function(fields, mappedFields, field) {
                var result = [];

                result.push(wbs.translateBirthdateToAgeGroup(this, field, "Age"));

                return result;
            }
        },
        "has-mental-illness-primary": {
            // Access1 only
            // WL Criteria Type field #4
            // Access1 eForm uses only primary diagnosis for "Mental Illness", but multiple for "Illness Diagnosis"
            "mode": "translateStrict",
            "htmlName": "has-mental-illness-primary",
            "eFormName": "Mental Illness",
            "htmlValues": ["adjustment-disorders", "anxiety-disorders", "delerium-dementia-amnestic-cognitive-disorders", "developmental-handicap", "childhood-adolescence-disorders", "dissociative-disorders", "eating-disorders", "factitious-disorders", "impulse-disorders", "mental-disorders-general-medical-conditions", "mood-disorders", "personality-disorders", "schizophrenia-other-psychotic-disorders", "sexual-gender-identity-disorders", "sleep-disorders", "somatoform-disorders", "substance-related-disorders", "no", "other", "unknown"],
            "eFormValues": ["formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "no-formal-diagnosis", "formal-diagnosis", "no-formal-diagnosis"]
        },
        "access1-has-mental-illness-diagnose": {
            // WL Criteria Type fields #5
            // TODO: Access1 eForm uses only primary diagnosis for "Mental Illness", but multiple for "Illness Diagnosis"
            // TODO: CASH eForm uses multiple diagnoses, "Mental health diagnosis"
            // TODO: normalize fields
            "mode": "translate",
            "htmlName": "has-mental-illness-primary",
            "eFormName": "Illness Diagnosis",
            "htmlValues": [],
            "eFormValues": []
        },
        "mental-illness-hpz-days": {
            // Access1 only
            // WL Criteria Type field #6
            "mode": "translate handled",
            "htmlName": "mental-illness-hpz-days",
            "eFormName": "Illness Hospitalization"
        },
        "legal-history": {
            // Access1 only
            // WL Criteria Type field #9
            // TODO: there is no input suitable for this translation
            // test-legal-history-1
            // test-legal-history-2
            "mode": "translate handled",
            "htmlName": "legal-history",
            "eFormName": "Legal History"
        },
        "currently-homeless": {
            // Access1 only
            // WL Criteria Type field #10
            "mode": "translate handled",
            "htmlName": "currently-homeless",
            "eFormName": "Residence"
        },
        "other-health-issues-has-concurrent-disorders": {
            // Access1 only
            // WL Criteria Type field #11
            // Part of multimapping from different fields to Other Health Issues
            "mode": "translateStrict",
            "htmlName": "has-concurrent-disorders",
            "eFormName": "Other Health Issues",
            "htmlValues": ["yes"],
            "eFormValues": ["concurrent-disorders"]
        },
        "other-health-issues-has-dual-diagnosis-illness-disability": {
            // Access1 only
            // WL Criteria Type field #11
            // Part of multimapping from different fields to Other Health Issues
            "mode": "translateStrict",
            "htmlName": "has-dual-diagnosis-illness-disability",
            "eFormName": "Other Health Issues",
            "htmlValues": ["yes"],
            "eFormValues": ["dual-diagnosis"]
        },
        "other-health-issues-has-neurological-illness-disability": {
            // Access1 only
            // WL Criteria Type field #11
            // Part of multimapping from different fields to Other Health Issues
            "mode": "customField",
            "htmlName": "has-neurological-illness-disability",
            "eFormName": "Other Health Issues",
            "custom": function(fields, mappedFields, field) {
                if (field.value === "yes") {
                    var result = [],
                        acquiredBrainInjury = wbm.translateHtmlToEFormStrict(this, field),
                        psychoGeriatricIssues = wbm.translateHtmlToEFormStrict(this, field);

                    // Copies the "yes" answer to two alternatives (the HTML form maps to both in one question)
                    acquiredBrainInjury.value = "acquired-brain-injury";
                    psychoGeriatricIssues.value = "psycho-geriatric-issues";

                    result.push(acquiredBrainInjury);
                    result.push(psychoGeriatricIssues);

                    return result;
                }
            }
        },
        "preferred-language": {
            // Access1 only
            // WL Criteria Type field #12
            "mode": "customField",
            "htmlName": "preferred-language",
            "eFormName": "Language",
            "htmlValues": ["eng", "fre", "other"],
            "eFormValues": ["english", "french", "other"],
            "custom": function(fields, mappedFields, field) {
                var translated = wbm.translateHtmlToEFormStrict(this, field);

                if (translated.value === undefined) {
                    translated.value = "other";
                }

                return translated;
            }
        }
    },
        wbm = window.Backend.Mapping,
        wbs = window.Backend.Shared;

    global.access1BackendMapping = function(fields, mappedFields) {
        var sharedMapped = wbs.Mapping(fields, mappedFields),
            mapped = wbm.backendMapping(access1Mappings, fields, mappedFields),
            allMapped = mappedFields.concat(sharedMapped, mapped);

        return allMapped;
    };
}(window, debug || console));