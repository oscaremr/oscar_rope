// Access 1: Common application form

/*jslint white: true, browser: true*/
/*global jQuery*/

(function($, global) {
    "use strict"; // jshint ;_;

    $(function() {
        // TODO: reuse simple (modal) templating, or use plugin from somewhere else

        function getTemplateAsObject($container) {
            var $placeholder = $("<div />");

            $placeholder.html($container.html());

            return $placeholder.children().first();
        }

        function buildModal(id) {
            var $modalTemplate = $("#modalTemplate"),
                $modal = getTemplateAsObject($modalTemplate);

            $modal.attr("id", id).appendTo($body).modal({
                show: false
            });

            return $modal;
        }

        function buildActtModal() {
            var $acttModal = buildModal("actt-modal"),
                $acttCriteria = $("#" + acttCriteriaId);

            $acttModal.find(".modal-header h3").html($acttCriteria.find("dt").html());
            $acttModal.find(".modal-body p").html($acttCriteria.find("dd").html());

            return $acttModal;
        }

        function getActtModal() {
            return $acttModal || buildActtModal();
        }

        var $body = $("body"),
            acttCriteriaId = "actt-specific-criteria",
            $acttModal;


        $("a").filterLinksToFragment(acttCriteriaId).click(function() {
            $acttModal = getActtModal();

            $acttModal.modal("show");

            return false;
        });
    });

}(jQuery, window));