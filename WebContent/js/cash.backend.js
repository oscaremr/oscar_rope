(function(global, debug) {
    var cashMappings = {
        "is-temp-housing-homeless": {
            // CASH only
            // WL Criteria Type field #15
            "mode": "translateStrict",
            "htmlName": "is-temp-housing-homeless",
            "eFormName": "Homeless",
            "htmlValues": ["yes", "no"],
            "eFormValues": ["homeless", "housed"]
        },
        "cash-has-mental-illness-diagnose": {
            // WL Criteria Type fields #16
            // TODO: Access1 eForm uses only primary diagnosis for "Mental Illness", but multiple for "Illness Diagnosis"
            // TODO: CASH eForm uses multiple diagnoses, "Mental health diagnosis"
            // TODO: normalize fields
            "mode": "translateStrict",
            "htmlName": "has-mental-illness-primary",
            "eFormName": "Mental health diagnosis",
            "htmlValues": ["adjustment-disorders", "anxiety-disorders", "delerium-dementia-amnestic-cognitive-disorders", "developmental-handicap", "childhood-adolescence-disorders", "dissociative-disorders", "eating-disorders", "factitious-disorders", "impulse-disorders", "mental-disorders-general-medical-conditions", "mood-disorders", "personality-disorders", "schizophrenia-other-psychotic-disorders", "sexual-gender-identity-disorders", "sleep-disorders", "somatoform-disorders", "substance-related-disorders", "no", "other", "unknown"],
            "eFormValues": ["formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "formal-diagnosis", "no-formal-diagnosis", "formal-diagnosis", "no-formal-diagnosis"]
        },
        "housing-type-presently-living-with": {
            // CASH only
            // WL Criteria Type field #17
            // eForm expects only one value, but there are multiple inputs
            "mode": "customFieldArray",
            "htmlName": "presently-living-with",
            "eFormName": "Housing type",
            "custom": function(fields, mappedFields, matchingFields) {
                var shared = undefined;

                $.each(matchingFields, function(index, field) {
                    switch (field.value) {
                    case "self":
                        shared = (shared === true);
                        break;

                    case "spouse-partner":
                    case "parents":
                    case "relatives":
                    case "non-relatives":
                    case "children":
                    case "other":
                        shared = true;
                        break;
                    }
                });

                if (shared !== undefined) {
                    var translated = wbm.createField(this.eFormName, (shared === false ? "independent" : "shared"));

                    return translated;
                }

                return undefined;
            }
        },
        "support-level": {
            // CASH only
            // WL Criteria Type field #19
            // TODO: there is no input suitable for this translation
            // test-level-1
            // test-level-2
            "mode": "translate handled",
            "htmlName": "support-level",
            "eFormName": "Support level"
        },
        "location-preferences": {
            // CASH only
            // WL Criteria Type field #20
            "mode": "translate handled",
            "htmlName": "location-preferences",
            "eFormName": "Geographic location"
        },
        "cash-age-years": {
            // WL Criteria Type fields #21 (duplicate mapping "age-years"/"date-of-birth")
            // TODO: use only one of age-years/date-of-birth
            "mode": "customField",
            "htmlName": "age-years",
            "eFormName": "age-years",
            "custom": function(fields, mappedFields, field) {
                var result = [];

                result.push(wbs.translateAgeInYearsFieldToAgeGroup(this, field, "Age category"));

                return result;
            }
        },
        "cash-date-of-birth": {
            // WL Criteria Type fields #21 (duplicate mapping "age-years"/"date-of-birth")
            // TODO: use only one of age-years/date-of-birth
            "mode": "customField",
            "htmlName": "date-of-birth",
            "eFormName": "date-of-birth",
            "custom": function(fields, mappedFields, field) {
                var result = [];

                result.push(wbs.translateBirthdateToAgeGroup(this, field, "Age category"));

                return result;
            }
        },
        "justice-involvement": {
            // CASH only
            // WL Criteria Type field #22
            // TODO: there is no input suitable for this translation
            // test-involvement-1
            // test-involvement-2
            "mode": "translate handled",
            "htmlName": "justice-involvement",
            "eFormName": "Justice Involvement"
        },
        "shppsu-criteria": {
            // CASH only
            // WL Criteria Type field #23
            // TODO: there is no input suitable for this translation
            // test-shppsu-criteria-1
            // test-shppsu-criteria-2
            "mode": "translate handled",
            "htmlName": "shppsu-criteria",
            "eFormName": "SHPPSU criteria"
        },
        "requires-accessible-housing": {
            // CASH only
            // WL Criteria Type field #24
            "mode": "translate handled",
            "htmlName": "requires-accessible-housing",
            "eFormName": "Accessible unit",
            "htmlValues": ["yes", "no"],
            "eFormValues": ["accessible-unit-required", "accessible-unit-not-required"]
        }
    },
        wbm = window.Backend.Mapping,
        wbs = window.Backend.Shared;

    global.cashBackendMapping = function(fields, mappedFields) {
        var sharedMapped = wbs.Mapping(fields, mappedFields),
            mapped = wbm.backendMapping(cashMappings, fields, mappedFields),
            allMapped = mappedFields.concat(sharedMapped, mapped);

        return allMapped;
    };
}(window, debug || console));