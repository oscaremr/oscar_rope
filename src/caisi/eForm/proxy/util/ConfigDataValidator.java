/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package caisi.eForm.proxy.util;

import caisi.eForm.proxy.exceptions.InvalidDataException;


/**
 * 
 * @author Kuldeep
 *
 */
public class ConfigDataValidator {
      
    public static String validateStringProperty(String key)throws InvalidDataException
    {
    	String value = ConfigData._properties.getProperty(key);
        if(value == null || "".equalsIgnoreCase(value) )
        {
            throw new InvalidDataException("Config Params are invalid: Invalid value in config file for field " + key);
        }
        return value;
    }

    public static int validateIntProperty(String key)throws InvalidDataException
    {
    	String value = ConfigData._properties.getProperty(key);
        int val;
        try
        {
            val = Integer.parseInt(value);
        }
        catch(NumberFormatException ex)
        {
            throw new InvalidDataException("Config Params are invalid: Invalid value in config file for field " + key);
        }
        return val;
    }

    public static Long validateLongProperty(String key)throws InvalidDataException
    {
    	String value = ConfigData._properties.getProperty(key);
        long val;
        try
        {
            val = Long.parseLong( value );
        }
        catch(NumberFormatException ex)
        {
            throw new InvalidDataException("Config Params are invalid: Invalid value in config file for field " + key);
        }
        return val;
    }
    
    public static boolean validateBooleanProperty(String key) throws InvalidDataException
    {
    	String value = ConfigData._properties.getProperty(key);
        boolean flag = false;
        if(value == null || "".equalsIgnoreCase(value))
        {
            throw new InvalidDataException("Config Params are invalid: Invalid value in config file for field " + key);
        }
        else
        {
            if(value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                flag = Boolean.parseBoolean(value);
            }
            else
            {
                throw new InvalidDataException("Config Params are invalid: Invalid value in config file for field " + key);
            }
        }
        return flag;
    }
    
    
}
