package caisi.eForm.proxy.util;

import java.util.Properties;

import caisi.eForm.proxy.exceptions.InvalidDataException;

/**
 * 
 * @author Kuldeep
 * 
 */
public class ConfigData {

	public static final Properties _properties = new Properties();
	public static final String HTTP_RESPONSE_200_OK = "HTTP/1.1 200 OK";
	
	public static String _server_Error_Message;
	public static String _client_Refferel_Success_Message;
	public static String _client_Refferel_Failure_Message;
	public static String _remote_App_Context;
	public static String _remote_App_Url;
	public static String _remote_Login_Url;
	public static String _form_Auth_Field_1_Name;
	public static String _form_Auth_Field_2_Name;
	public static String _form_Auth_Field_3_Name;
	public static String _anonymous_User_Name;
	public static String _anonymous_User_Password;
	public static String _anonymous_User_Pin;
	public static String _remote_Host_IP;
	public static int _remote_Host_Port;
	public static String _remote_Host_Scheme;
	public static String _display_Form_Remote_URL_For_Remote;
	public static String _add_Form_Local_URL;
	public static String _add_Form_Remote_URL_For_Local;
	public static String _add_Form_Remote_URL_For_Remote;
	public static String _client_refferel_remote_url;
	public static String _client_refferel_param_demographic_no;
	public static String _client_refferel_param_program_id;
	public static String _client_refferel_param_program_id_value;
	public static String _client_refferel_param_reason;
	public static String _client_refferel_param_reason_value;
	public static String _client_refferel_param_method;
	public static String _client_refferel_param_method_value;
	public static String _client_refferel_param_view_tab;
	public static String _client_refferel_param_view_tab_value;
	public static String _document_dir;
	public static String _upload_form_remote_url;
	public static String _param_fid;
	public static String _param_demographic_no;
	public static String _param_appointment;
	public static String _facility_id;
	//public static String _bedCommunityProgramId;
	public static String _serviceProgramId;
	public static String _eform_first_name;
	public static String _eform_last_name;
	public static String _eform_date_of_birth;
	public static String _eform_gender;
	public static String _eform_residential_status;	
	public static String _new_client_first_name;
	public static String _new_client_last_name;
	public static String _new_client_date_of_birth;
	public static String _new_client_gender;
	public static String _new_client_patient_status;
	public static String _new_client_residential_status;
	public static String _new_client_service_program;
	public static String _new_client_save_url;
	public static String _trigger_email_url;
	
	
	public static void validateConfigData() throws InvalidDataException {

		try {
			_remote_Host_IP = ConfigDataValidator.validateStringProperty("REMOTE_HOST_IP");
			_remote_Host_Port = ConfigDataValidator.validateIntProperty("REMOTE_HOST_PORT");
			_remote_Host_Scheme = ConfigDataValidator.validateStringProperty("REMOTE_HOST_SCHEME");

			_remote_App_Context = ConfigDataValidator.validateStringProperty("REMOTE_APP_CONTEXT");
			_remote_App_Url = ConfigDataValidator.validateStringProperty("REMOTE_APP_URL");
			_remote_Login_Url = ConfigDataValidator.validateStringProperty("REMOTE_LOGIN_URL");

			_form_Auth_Field_1_Name = ConfigDataValidator.validateStringProperty("FORM_AUTH_FIELD_1_NAME");
			_form_Auth_Field_2_Name = ConfigDataValidator.validateStringProperty("FORM_AUTH_FIELD_2_NAME");
			_form_Auth_Field_3_Name = ConfigDataValidator.validateStringProperty("FORM_AUTH_FIELD_3_NAME");

			_anonymous_User_Name = ConfigDataValidator.validateStringProperty("ANONYMOUS_USER_NAME");
			_anonymous_User_Password = ConfigDataValidator.validateStringProperty("ANONYMOUS_USER_PASSWORD");
			_anonymous_User_Pin = ConfigDataValidator.validateStringProperty("ANONYMOUS_USER_PIN");

			_display_Form_Remote_URL_For_Remote = ConfigDataValidator
					.validateStringProperty("DISPLAY_FORM_REMOTE_URL_FOR_REMOTE");
			_add_Form_Local_URL = ConfigDataValidator.validateStringProperty("ADD_FORM_LOCAL_URL");

			_add_Form_Remote_URL_For_Local = ConfigDataValidator.validateStringProperty("ADD_FORM_REMOTE_URL_FOR_LOCAL");
			_add_Form_Remote_URL_For_Remote = ConfigDataValidator
					.validateStringProperty("ADD_FORM_REMOTE_URL_FOR_REMOTE");
			
			_client_refferel_remote_url = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_REMOTE_URL");
			
			_client_refferel_param_demographic_no = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_DEMOGRAPHIC_NO");
			
			_client_refferel_param_program_id = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_PROGRAM_ID");
			_client_refferel_param_program_id_value = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_PROGRAM_ID_VALUE");
			
			_client_refferel_param_method = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_METHOD");
			_client_refferel_param_method_value = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_METHOD_VALUE");
			
			_client_refferel_param_reason = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_REASON");
			_client_refferel_param_reason_value = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_REASON_VALUE");
			
			_client_refferel_param_view_tab = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_VIEW_TAB");
			_client_refferel_param_view_tab_value = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_PARAM_VIEW_TAB_VALUE");
			
			_document_dir = ConfigDataValidator.validateStringProperty("DOCUMENT_DIR");

			_upload_form_remote_url = ConfigDataValidator.validateStringProperty("UPLOAD_FORM_REMOTE_URL");

			_param_fid = ConfigDataValidator.validateStringProperty("PARAM_FID");
			_param_demographic_no = ConfigDataValidator.validateStringProperty("PARAM_DEMOGRAPHIC_NO");
			_param_appointment = ConfigDataValidator.validateStringProperty("PARAM_APPOINTMENT");
			_facility_id = ConfigDataValidator.validateStringProperty("FACILITY_ID");
			//_bedCommunityProgramId = ConfigDataValidator.validateStringProperty("BED_COMMUNITY_PROGRAM_ID");
			_serviceProgramId = ConfigDataValidator.validateStringProperty("SERVICE_PROGRAM_ID");

			_eform_first_name = ConfigDataValidator.validateStringProperty("EFORM_FIRST_NAME");
			_eform_last_name = ConfigDataValidator.validateStringProperty("EFORM_LAST_NAME");
			_eform_date_of_birth = ConfigDataValidator.validateStringProperty("EFORM_DATE_OF_BIRTH");
			_eform_gender = ConfigDataValidator.validateStringProperty("EFORM_GENDER");
			_eform_residential_status = ConfigDataValidator.validateStringProperty("EFORM_RESIDENTIAL_STATUS");
			_new_client_first_name = ConfigDataValidator.validateStringProperty("NEW_CLIENT_FIRST_NAME");
			_new_client_last_name = ConfigDataValidator.validateStringProperty("NEW_CLIENT_LAST_NAME");
			_new_client_date_of_birth = ConfigDataValidator.validateStringProperty("NEW_CLIENT_DATE_OF_BIRTH");
			_new_client_gender = ConfigDataValidator.validateStringProperty("NEW_CLIENT_GENDER");
			_new_client_residential_status = ConfigDataValidator.validateStringProperty("NEW_CLIENT_RESIDENTIAL_STATUS");
			_new_client_patient_status = ConfigDataValidator.validateStringProperty("NEW_CLIENT_PATIENT_STATUS");
			_new_client_service_program = ConfigDataValidator.validateStringProperty("NEW_CLIENT_SERVICE_PROGRAM");
			_new_client_save_url = ConfigDataValidator.validateStringProperty("NEW_CLIENT_SAVE_URL");
			
			_server_Error_Message = ConfigDataValidator.validateStringProperty("SERVER_ERROR_MESSAGE");
			_client_Refferel_Success_Message = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_SUCCESS_MESSAGE");
			_client_Refferel_Failure_Message = ConfigDataValidator.validateStringProperty("CLIENT_REFFEREL_FAILURE_MESSAGE");
			_trigger_email_url = ConfigDataValidator.validateStringProperty("SEND_EMAIL_TO_NOTIFY_WL_OPERATOR_URL");
			
		} catch (InvalidDataException e) {
			EFormProxyUtility._proxyLogger.error("Error while validating config data.");
			EFormProxyUtility._proxyLogger.error(e);
			e.printStackTrace();
			throw e;
		}
	}
}
