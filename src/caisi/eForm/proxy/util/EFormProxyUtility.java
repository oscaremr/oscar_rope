package caisi.eForm.proxy.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;

import caisi.eForm.proxy.constants.ProxyAppConstants;
import caisi.eForm.proxy.exceptions.InvalidDataException;

/**
 * 
 * @author Kuldeep
 *
 */
public class EFormProxyUtility {
	public static Logger _proxyLogger = null;
	private static Logger log = Logger.getLogger(EFormProxyUtility.class);

	public static HttpClient validateAndGetHTTPClient (HttpServletRequest request) throws InvalidDataException {
		if (request.getSession().getAttribute(ProxyAppConstants.IS_USER_AUTHENTICATED) == null
				|| (Boolean) request.getSession().getAttribute(ProxyAppConstants.IS_USER_AUTHENTICATED) == false) {
			log.error("Unauthenticated user");
			throw new InvalidDataException("Unauthenticated user");

		}
		if (request.getSession().getAttribute(ProxyAppConstants.HTTP_SESSION) == null) {
			log.error("HTTP Client not found");
			throw new InvalidDataException("HTTP Client not found");
		}
		HttpClient httpClient = (HttpClient) request.getSession().getAttribute(ProxyAppConstants.HTTP_SESSION);
		
		if (request.getSession().getAttribute(ProxyAppConstants.HEADER_SET_COOKIE) == null) {
			log.error("HTTP Session cookie not found. Login failed.");
			throw new InvalidDataException("HTTP Session cookie not found");
		}
		
		log.info("Validation done.");
		return httpClient;
	}

	public static String getFileName(String name) {
		if (name != null) {
			int idx = name.lastIndexOf(".");
			if (idx != -1) {
				return name.substring(0, idx);
			}
		}
		return name;
	}
	
	public static String getFileExtension(String name) {
		if (name != null) {
			int idx = name.lastIndexOf(".");
			if (idx != -1) {
				return name.substring(idx+1,name.length());
			} else {
				return null;
			}
		}
		return null;
	}
	
	public static void sendErrorMessageToClient(HttpServletResponse response, String errorMsg) throws IOException
	{
		response.setContentType("text/html");
		//send the output to browser
		PrintWriter writer = response.getWriter();
		writer.print( getHTMLMessage(errorMsg));
		writer.flush();
		writer.close();
	}
	
	public static String getHTMLMessage(String errorMessage)
	{
		String HTML_HEADER = "<HTML><HEAD><TITLE></TITLE></HEAD><BODY><CENTER><FONT SIZE='3'>";
		String HTML_FOOTER = "</FORT></CENTER></BODY></HTML>";
		
		return ( HTML_HEADER + errorMessage + HTML_FOOTER );
	
	}
}
