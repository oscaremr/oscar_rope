package caisi.eForm.proxy;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;

import caisi.eForm.proxy.constants.ProxyAppConstants;
import caisi.eForm.proxy.exceptions.InvalidDataException;
import caisi.eForm.proxy.util.ConfigData;
import caisi.eForm.proxy.util.EFormProxyUtility;

public class AddEForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(AddEForm.class);
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("doPost");
		saveEForm(request, response);
	}

	private void saveEForm(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			HttpClient httpClient = EFormProxyUtility.validateAndGetHTTPClient(request);
			Header[] headers = (Header[]) request.getSession().getAttribute(ProxyAppConstants.HEADER_SET_COOKIE);
			
			log.info("Validation done. Submitting the form to caisi server.");
			//1. create new client.			
			String demographicNoWith = HttpClientProcessor.getInstance().createNewClient(httpClient, headers, request.getParameterMap());	
			if(demographicNoWith == null || demographicNoWith.equals("")){
				log.error("Creating the new client not successful.");
				throw new InvalidDataException("Creating new client failed.");
			}
			
			String demographicNo = demographicNoWith.split("<")[0];
			//just to validate if response is a proper number. Will throw NumberFormatException otherwise
			Integer.parseInt(demographicNo);
		
			//2. save eform data.
			List<String> uploadFileNames = (List<String>) request.getSession().getAttribute(ProxyAppConstants.UPLOAD_FILE_NAME_LIST);
			
			String eFormText = HttpClientProcessor.getInstance().persistFormData(httpClient, headers, request.getParameterMap(), request.getQueryString(), demographicNo, uploadFileNames);	
			log.debug(eFormText);
			
			response.setContentType("text/html");
			PrintWriter writer = response.getWriter();
			writer.print(eFormText);
			writer.flush();
			writer.close();

			//remove the session attributes.
			request.getSession().invalidate();
		} catch (NumberFormatException e) {
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			log.error(e);
			e.printStackTrace();
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		} catch (InvalidDataException e) {
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			log.error(e);
			e.printStackTrace();
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		} catch (ClientProtocolException e) {
			response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
			log.error(e);
			e.printStackTrace();
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		} catch (ConnectException e) {
			e.printStackTrace();
			log.error(e);
			response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		}catch (IOException e) {
			response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
			log.error(e);
			e.printStackTrace();
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		} catch (Exception e) {
			response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
			log.error(e);
			e.printStackTrace();
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		}
	}
}
