package caisi.eForm.proxy;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import caisi.eForm.proxy.constants.ProxyAppConstants;
import caisi.eForm.proxy.exceptions.InvalidDataException;
import caisi.eForm.proxy.util.ConfigData;
import caisi.eForm.proxy.util.EFormProxyUtility;

public class ApplicationServletContextListener implements ServletContextListener {
	public void contextInitialized(ServletContextEvent event) {
		ServletContext ctx = event.getServletContext();
		loadPropertyFiles(ctx);
	}

	public void contextDestroyed(ServletContextEvent event) {

	}

	private void loadPropertyFiles(ServletContext ctx) {

		String logFilePath = ctx.getInitParameter(ProxyAppConstants.PROXY_LOG4J_FILE);
		String completeLogFilePath = ctx.getRealPath(logFilePath);
		PropertyConfigurator.configure(completeLogFilePath);
		EFormProxyUtility._proxyLogger = Logger.getLogger(getClass().getName());
		System.out.println("Log File Path: " + completeLogFilePath);
		EFormProxyUtility._proxyLogger.info("Log4j Property file " + completeLogFilePath + " loaded successfully.");

		String proxyAppConfig = ctx.getInitParameter(ProxyAppConstants.PROXY_CONFIG_FILE);
		InputStream inputStream = ctx.getResourceAsStream(proxyAppConfig);
		try {
			ConfigData._properties.load(inputStream);
			EFormProxyUtility._proxyLogger.info("Proxy config property file: " + proxyAppConfig
					+ " loaded successfully");

			ConfigData.validateConfigData();

		} catch (IOException e) {
			EFormProxyUtility._proxyLogger.error("Error while loading property file: " + proxyAppConfig);
			e.printStackTrace();
		} catch (InvalidDataException e) {
			EFormProxyUtility._proxyLogger.error("Error while validating property file data: " + proxyAppConfig);
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				//ignore
			}
		}

	}
}
