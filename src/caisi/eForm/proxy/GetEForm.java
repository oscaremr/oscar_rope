package caisi.eForm.proxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import caisi.eForm.proxy.constants.ProxyAppConstants;
import caisi.eForm.proxy.exceptions.InvalidDataException;
import caisi.eForm.proxy.exceptions.InvalidLoginArgumentException;
import caisi.eForm.proxy.util.ConfigData;
import caisi.eForm.proxy.util.EFormProxyUtility;

public class GetEForm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(GetEForm.class);
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("doGet");
		getEForm(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("doPost");
		doGet(request, response);
	}

	private void getEForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		
		//workaround for invalid SSL certificates in dev service.
		String  isDevServiceSSL = getInitParameter("is_devlopment_service_ssl");
		boolean isDev = Boolean.parseBoolean(isDevServiceSSL);
		if (isDev) {
			httpClient = WebClientSSLWrapper.wrapClient(httpClient);
		}

		Header[] headers = null;
		try {
			//1. do login
			headers = HttpClientProcessor.getInstance().doAuthentication(httpClient);
			if (headers == null || headers.length == 0) {
				log.error("HTTP Session cookie not found. Login failed.");
				throw new InvalidDataException("Login failed.");
			}
			request.getSession().setAttribute(ProxyAppConstants.HEADER_SET_COOKIE, headers);
			request.getSession().setAttribute(ProxyAppConstants.IS_USER_AUTHENTICATED, true);
			// Save httpClient in session for future communications
			request.getSession().setAttribute(ProxyAppConstants.HTTP_SESSION, httpClient);
			
			//2. set facility
			HttpClientProcessor.getInstance().setFacilityId(httpClient, headers);
			
			//3. get eform
			String eFormText = HttpClientProcessor.getInstance().getEFormAsHTMLText(httpClient, headers);
			log.debug(eFormText);			
			
			//4. add upload functionality to eform
			eFormText = addUploadFunctionality(eFormText);
			
			log.debug(eFormText);
							
			
			response.setContentType("text/html");
			//send the output to browser
			PrintWriter writer = response.getWriter();
			writer.print(eFormText);
			writer.flush();
			writer.close();
			log.info("Finished processing getEForm.");
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			log.error(e);
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		}catch (ConnectException e) {
			e.printStackTrace();
			log.error(e);
			response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		}catch (InvalidLoginArgumentException e) {
			e.printStackTrace();
			log.error(e);
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		}   catch (IOException e) {
			e.printStackTrace();
			log.error(e);
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
			EFormProxyUtility.sendErrorMessageToClient(response, ConfigData._server_Error_Message);
		}
	}

	private String addUploadFunctionality(String eFormText) throws IOException {
		String htmlTag = ProxyAppConstants.HTML_FORM_TAG;
		String  replaceText = ProxyAppConstants.UPLOAD_DETAILS_PLACEHOLDER;
		eFormText =  injectHTMLCodeBeforeTag(replaceText, eFormText, htmlTag);
		
//		htmlTag = ProxyAppConstants.HTML_HEAD_TAG;
//		String  fileName = getInitParameter("upload_include_head");
//		eFormText =  injectHTMLCodeBeforeTag(getTextFromFile(fileName), eFormText, htmlTag);
		
		htmlTag = ProxyAppConstants.HTML_BODY_TAG;
		String uploadText = getTextFromFile(getInitParameter("upload_include_head")) + " "+ getTextFromFile(getInitParameter("upload_form_body"));
		eFormText = injectHTMLCodeBeforeTag(uploadText, eFormText, htmlTag);
		
		return eFormText;
	}

	private String injectHTMLCodeBeforeTag(String replaceText, String eFormText, String htmlTag) throws IOException {
		if (!"".equals(replaceText)) {
			eFormText.replaceAll(htmlTag.toUpperCase(), htmlTag);
			if(eFormText.lastIndexOf(htmlTag) != -1){
				String part1 = eFormText.substring(0, eFormText.lastIndexOf(htmlTag) - 1);
				String part2 = replaceText;
				String part3 = eFormText.substring(eFormText.lastIndexOf(htmlTag));
				eFormText = part1 + part2 + part3;
			}
		}

		return eFormText;
	}
	
	private String getTextFromFile (String fileName) throws IOException{
		String replaceText = "";
		InputStream inp = null;
		try{
			inp = getServletContext().getResourceAsStream(fileName);
			if (inp != null) {
				InputStreamReader isr = new InputStreamReader(inp);
				BufferedReader reader = new BufferedReader(isr);
				String line;
				while ((line = reader.readLine()) != null) {
					replaceText = replaceText + " \n " + line;
				}
			}
		} finally {
			if(inp != null){
				inp.close();
			}
		}
		
		return replaceText;
	}
}






