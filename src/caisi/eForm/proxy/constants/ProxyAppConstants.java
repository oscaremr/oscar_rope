package caisi.eForm.proxy.constants;

/**
 * 
 * @author Kuldeep
 *
 */
public class ProxyAppConstants {

	public static final String PROXY_LOG4J_FILE = "proxy-log4j-file";
	public static final String PROXY_CONFIG_FILE = "proxy-config-file";
	
	//public static final String LOGON_SITE = "localhost";
    //public static final int    LOGON_PORT = 8080;
    
    public static final String IS_USER_AUTHENTICATED = "IsUserAuthenticated";
    public static final String HEADER_SET_COOKIE = "Set-Cookie";
    public static final String HTTP_SESSION = "HTTP_SESSION";
    
    public static final String AJAX_ERROR_MSG = "<div id=\"error\">error</div><div id=\"message\">Error in file upload</div>";
    
    public static final String HTML_FORM_TAG = "</form>";
    public static final String HTML_HEAD_TAG = "</head>";
    public static final String HTML_BODY_TAG = "</body>";
    public static final String UPLOAD_DETAILS_PLACEHOLDER = "<div id='upload_details'></div>";
    public static final String UPLOAD_FILE_FIELD = "uploadFileName";
    public static final String FORM_ACTION_NAME = "login";
    public static final String PARAM_NAME_FID = "fid";
    public static final String PARAM_NAME_DEMOGRAPHIC_NO = "demographic_no";
    public static final String PARAM_NAME_APPOINTMENT = "appointment";
    public static final String PARAM_NAME_EFMDEMOGRAPHIC_NO = "efmdemographic_no";
    public static final String UPLOAD_FILE_NAME_LIST = "UploadFileNameList";
}
