package caisi.eForm.proxy;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.http.client.HttpClient;

import caisi.eForm.proxy.constants.ProxyAppConstants;

public class ProxyHTTPSessionListener implements HttpSessionListener {
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("ProxyHTTPSessionListner.sessionDestroyed()");
		HttpSession session = se.getSession();

		if(session != null){
			//deallocate the httpClient resources.
			HttpClient httpClient = (HttpClient) session.getAttribute(ProxyAppConstants.HTTP_SESSION);
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
			}
		}
	}

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		// TODO Auto-generated method stub

	}
}
