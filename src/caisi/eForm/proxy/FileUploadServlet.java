package caisi.eForm.proxy;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;

import caisi.eForm.proxy.constants.ProxyAppConstants;
import caisi.eForm.proxy.exceptions.InvalidDataException;
import caisi.eForm.proxy.util.ConfigData;
import caisi.eForm.proxy.util.EFormProxyUtility;

public class FileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(FileUploadServlet.class);
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String responseText = null;
		String successMsg = "<div id=\"status\">success</div> <div id=\"message\">Uploaded Successfully</div>";
		String errorMsg = "<div id=\"error\">error</div><div id=\"message\">Error in file upload</div>";
		try {
			HttpClient httpClient = EFormProxyUtility.validateAndGetHTTPClient(request);
			log.info("Saving the attachment to proxy.");
			String uploadedFileName = saveUploadedFile(request, response);
			
			List<String> fileNames = (List<String>) request.getSession().getAttribute(ProxyAppConstants.UPLOAD_FILE_NAME_LIST);
			if(fileNames == null){
				fileNames = new ArrayList<String>();
			}
			fileNames.add(uploadedFileName);
			request.getSession().setAttribute(ProxyAppConstants.UPLOAD_FILE_NAME_LIST, fileNames);
			
//			Header[] headers = (Header[]) request.getSession().getAttribute(ProxyAppConstants.HEADER_SET_COOKIE);
//			
//			log.info("Sending upload file details to CAISI server.");
//			responseText = HttpClientProcessor.getInstance().uploadFormAttachmentData(httpClient, headers, request.getParameterMap(), uploadedFileName);
//			log.debug(responseText);
			responseText = successMsg;
			
			
		} catch (InvalidDataException e) {
			responseText = handleException(e, response);
			responseText = errorMsg;
		} catch (ClientProtocolException e) {
			responseText = handleException(e, response);
			responseText = errorMsg;
		} catch (IOException e) {
			responseText = handleException(e, response);
			responseText = errorMsg;
		} catch (FileUploadException e) {
			responseText = handleException(e, response);
			responseText = errorMsg;
		} catch (Exception e) {
			responseText = handleException(e, response);
			responseText = errorMsg;
		}
		
		PrintWriter writer = response.getWriter();
		writer.print(responseText);
		writer.flush();
		writer.close();
	}
	private String saveUploadedFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// create file upload factory and upload servlet
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List uploadedItems = null;
		FileItem fileItem = null;
		String filePath = ConfigData._document_dir;
		String docFileName = null;
		// iterate over all uploaded files
		uploadedItems = upload.parseRequest(request);
		Iterator i = uploadedItems.iterator();
		while (i.hasNext()) {
			fileItem = (FileItem) i.next();
			if (fileItem.isFormField() == false) {
				if (fileItem.getSize() > 0) {
					String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName
							.lastIndexOf("\\") > 0) ? "\\" : "/";
					int startIndex = myFullFileName.lastIndexOf(slashType);

					// Ignore the path and get the filename
					myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
					docFileName = EFormProxyUtility.getFileName(myFileName) + "." + new Date().getTime() + "."
							+ EFormProxyUtility.getFileExtension(myFileName);

					File uploadedFile = new File(filePath, docFileName);
					fileItem.write(uploadedFile);
				}
			}
		}
		return docFileName;
	}

	private String handleException(Exception e, HttpServletResponse response) {
		String responseText;
		responseText = ProxyAppConstants.AJAX_ERROR_MSG;
		log.error(e);
		e.printStackTrace();
		response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);
		return responseText;
	}
}
