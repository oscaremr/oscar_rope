package caisi.eForm.proxy.exceptions;


public class InvalidDataException extends Exception{
	
	String error;
	
	public InvalidDataException(String err)
	{
		super(err);    
		error = err;  
	}
	
	public String getError()
	{
		return error;
	}

	
}
