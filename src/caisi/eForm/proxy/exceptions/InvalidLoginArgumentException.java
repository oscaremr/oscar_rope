package caisi.eForm.proxy.exceptions;

public class InvalidLoginArgumentException extends Exception {

	String error;
	
	public InvalidLoginArgumentException(String error) {
		// TODO Auto-generated constructor stub
		super(error);
		this.error = error;
	}
	
	public String getError()
	{
		return error;
	}
}
