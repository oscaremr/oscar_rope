package caisi.eForm.proxy;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import caisi.eForm.proxy.constants.ProxyAppConstants;
import caisi.eForm.proxy.exceptions.InvalidDataException;
import caisi.eForm.proxy.exceptions.InvalidLoginArgumentException;
import caisi.eForm.proxy.util.ConfigData;
import caisi.eForm.proxy.util.EFormProxyUtility;

public class HttpClientProcessor extends ProxyAppConstants {
	private static HttpClientProcessor instance = null;
	private static Logger log = Logger.getLogger(HttpClientProcessor.class);
	private static Map <String, String> newClientRequiredFields = new HashMap <String, String>();
	private HttpClientProcessor() {
		newClientRequiredFields.put(ConfigData._eform_first_name, ConfigData._new_client_first_name);
		newClientRequiredFields.put(ConfigData._eform_last_name, ConfigData._new_client_last_name);
		newClientRequiredFields.put(ConfigData._eform_date_of_birth, ConfigData._new_client_date_of_birth);
		newClientRequiredFields.put(ConfigData._eform_gender, ConfigData._new_client_gender);
		newClientRequiredFields.put(ConfigData._eform_residential_status, ConfigData._new_client_residential_status);
	}

	public static HttpClientProcessor getInstance() {
		if (instance == null) {
			instance = new HttpClientProcessor();
		}
		return instance;
	}

	public Header[] doAuthentication(HttpClient httpClient) throws ClientProtocolException, IOException,
			InvalidLoginArgumentException, URISyntaxException {
		log.debug("In doAuthentication");
		HttpPost httpost = null;
		HttpGet httpget = null;
		Header[] headers = null;
		try {
			// 1: Try to get Index page
			httpget = new HttpGet(getRemoteURI(ConfigData._remote_App_Url));
			HttpResponse response = httpClient.execute(httpget);
			HttpEntity entity = response.getEntity();

			System.out.println("Login form get: " + response.getStatusLine());
			EntityUtils.consume(entity);

			// 2: Get login page
			httpost = new HttpPost(getRemoteURI(ConfigData._remote_Login_Url));
			List<org.apache.http.NameValuePair> nvps = new ArrayList<org.apache.http.NameValuePair>();
			nvps.add(new BasicNameValuePair("action", FORM_ACTION_NAME));
			nvps.add(new BasicNameValuePair(ConfigData._form_Auth_Field_1_Name, ConfigData._anonymous_User_Name));
			nvps.add(new BasicNameValuePair(ConfigData._form_Auth_Field_2_Name, ConfigData._anonymous_User_Password));
			nvps.add(new BasicNameValuePair(ConfigData._form_Auth_Field_3_Name, ConfigData._anonymous_User_Pin));

			httpost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));

			response = httpClient.execute(httpost);
			entity = response.getEntity();

			System.out.println("Login form get: " + response.getStatusLine());
			EntityUtils.consume(entity);

			headers = response.getHeaders(HEADER_SET_COOKIE);
			if (headers == null || headers.length == 0 || headers[0] == null || ("").equals(headers[0].getValue())) {
				throw new InvalidLoginArgumentException("Invalid login arguments");
			}
			log.info(headers[0].getValue());
		} finally {
			if (httpget != null) {
				httpget.releaseConnection();
			}
			if (httpost != null) {
				httpost.releaseConnection();
			}
		}

		return headers;
	}

	public void setFacilityId(HttpClient httpClient, Header[] headers) throws ClientProtocolException,
			IOException, URISyntaxException {
		log.debug("In getEFormAsHTMLText");
		String htmlResponseText = null;
		HttpGet httpget = null;
		try {	
			URIBuilder builder = new URIBuilder();
			builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
			.setPath(ConfigData._remote_App_Context + ConfigData._remote_Login_Url);
			String uriString = builder.build().toString();
			uriString = uriString + "?" + "nextPage=caisiPMM&selectedFacilityId=" + ConfigData._facility_id;
			
			httpget = new HttpGet(uriString);
			
			
			httpget.setHeaders(headers);
			htmlResponseText = EntityUtils.toString(httpClient.execute(httpget).getEntity(), Consts.UTF_8);
			System.out.println("Set the facility id.");
			log.debug(htmlResponseText);
		} finally {
			if (httpget != null) {
				httpget.releaseConnection();
			}
		}
	}
	
	public String getEFormAsHTMLText(HttpClient httpClient, Header[] headers) throws ClientProtocolException,
			IOException, URISyntaxException {
		log.debug("In getEFormAsHTMLText");
		String htmlResponseText = null;
		HttpGet httpget = null;
		try {
			//create the url
			URIBuilder builder = new URIBuilder();
			builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
			.setPath(ConfigData._remote_App_Context + ConfigData._display_Form_Remote_URL_For_Remote)
			.setParameter(PARAM_NAME_FID,ConfigData._param_fid)
			.setParameter(PARAM_NAME_DEMOGRAPHIC_NO,ConfigData._param_demographic_no)
			.setParameter(PARAM_NAME_APPOINTMENT,ConfigData._param_appointment);
			URI uri = builder.build();
			log.debug(uri.toString());
			
			httpget = new HttpGet(uri);
			httpget.setHeaders(headers);
			htmlResponseText = EntityUtils.toString(httpClient.execute(httpget).getEntity(), Consts.UTF_8);
			System.out.println("Got eform from server");
		} finally {
			if (httpget != null) {
				httpget.releaseConnection();
			}
		}
		//change path ../eform/displayImage.do? to http://localhost:8080/oscar/
		//e.g. href="../eform/displayImage.do?imagefile=bootstrap.min.css"
		//should be href="../css/bootstrap.min.css"
	    /*String path = ConfigData._remote_Host_Scheme ;  //http
	    path = path.concat("://"); //http://
	    path = path.concat(ConfigData._remote_Host_IP); //http://localhost
	    path = path.concat(":"); //http://localhost:
	    path = path.concat(String.valueOf(ConfigData._remote_Host_Port)); //http://localhost:8080/	    
	    path = path.concat(ConfigData._remote_App_Context); //http://localhost:8080/oscar	  
	    
	    String path_displayImage = path.concat("/eform/displayImage.do"); //http://localhost:8080/oscar/eform/displayImage.do?
	    String path_css = path.concat("/css");
	    String path_js = path.concat("/js");	    
	    String path_attachForm = path.concat("/eform/eFormAttachmentForm.do");
	    String path_addEform = path.concat("/eform/addEForm.do");
	    */
	   htmlResponseText = htmlResponseText.replace("script src=\"../eform/displayImage.do?imagefile=", "script src=\"../js/");
	    htmlResponseText = htmlResponseText.replace("href=\"../eform/displayImage.do?imagefile=", "href=\"../css/");
	  
	    //  htmlResponseText = htmlResponseText.replaceAll("../css", path_css);
	  //  htmlResponseText = htmlResponseText.replaceAll("../js", path_js);
	  //  htmlResponseText = htmlResponseText.replaceAll("../eform/eFormAttachmentForm.do?", path_attachForm);
	  //  htmlResponseText = htmlResponseText.replaceAll("../eform/addEForm.do?", path_addEform);
	
		return htmlResponseText;
	}

	public String createNewClient(HttpClient httpClient, Header[] headers,
			Map<String, String[]> requestParameters) throws ClientProtocolException, IOException, URISyntaxException {
		log.debug("In createNewClient");
		
		//setFacilityId(httpClient, headers);
		
		URIBuilder builder = new URIBuilder();
		builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
		.setPath(ConfigData._remote_App_Context + ConfigData._new_client_save_url);
		String uriString = builder.build().toString();
		uriString = uriString + "?" + "method=create&type=quick";
		HttpGet httpget = null;
		try {
			httpget = new HttpGet(uriString);
			httpget.setHeaders(headers);
			HttpResponse response = httpClient.execute(httpget);
			EntityUtils.consume(response.getEntity());
		} finally {
			if (httpget != null) {
				httpget.releaseConnection();
			}
		}
		String htmlResponseText = null;
		List<NameValuePair> nameValueList = new ArrayList<NameValuePair>();

		for (Entry<String, String[]> entry : requestParameters.entrySet()) {
			String key = entry.getKey();
			String[] values = entry.getValue();
			if(key != null && !"".equals(key) && values != null && values.length > 0){
				String value = values[0];
				if(newClientRequiredFields.containsKey(key)){
					if(ConfigData._eform_gender.equals(key)){
						value = mapGenderCode(value);
					}
					nameValueList.add(new BasicNameValuePair(newClientRequiredFields.get(key), value));
				}
			}
		}
		//The default patient status is active
		nameValueList.add(new BasicNameValuePair(ConfigData._new_client_patient_status, "AC"));
		
		//nameValueList.add(new BasicNameValuePair(ConfigData._new_client_residential_status, ConfigData._bedCommunityProgramId));
		nameValueList.add(new BasicNameValuePair(ConfigData._new_client_service_program, ConfigData._serviceProgramId));
		nameValueList.add(new BasicNameValuePair("method", "save_proxy"));
		nameValueList.add(new BasicNameValuePair("type", "quick"));
		nameValueList.add(new BasicNameValuePair("client.hin", ""));
		
		//create the url
		builder = new URIBuilder();
		builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
		.setPath(ConfigData._remote_App_Context + ConfigData._new_client_save_url);
		uriString = builder.build().toString();
		//uriString = uriString + "?" + "method=save_proxy&type=quick";
		log.debug(uriString);
		
		HttpPost httpost = new HttpPost(uriString);
		httpost.setEntity(new UrlEncodedFormEntity(nameValueList, Consts.UTF_8));
		httpost.setHeaders(headers);

		try {
			System.out.println("Uploading eform to server...");
			htmlResponseText = EntityUtils.toString(httpClient.execute(httpost).getEntity(), Consts.UTF_8);
			log.info("htmlResponseText: " + htmlResponseText);
			System.out.println("Uploaded eform to server.");
		} finally {
			httpost.releaseConnection();
		}

		return htmlResponseText;
	}

	private String mapGenderCode(String value) {
		if(value.equals("male")){
			value = "M";
		} else if(value.equals("female")){
			value = "F";
		} else if(value.equals("transgender")){
			value = "T";
		} else {
			value = "O";
		}
		return value;
	}
	
	public String persistFormData(HttpClient httpClient, Header[] headers,
			Map<String, String[]> requestParameters, String queryString, String demographicNo, List<String> uploadFileNames) throws ClientProtocolException, IOException, URISyntaxException {
		log.debug("In persistFormData");
		String htmlResponseText = null;
		
		List<NameValuePair> nameValueList = new ArrayList<NameValuePair>();

		for (Entry<String, String[]> entry : requestParameters.entrySet()) {
			String key = entry.getKey();
			String[] values = entry.getValue();
			if(key != null && !"".equals(key) && values != null && values.length > 0){
				nameValueList.add(new BasicNameValuePair(key, values[0]));
			}
		}

		int i = 0;
		if(uploadFileNames != null){
			for (String uploadFileName : uploadFileNames) {
				String uploadDocId = uploadFormAttachmentData(httpClient, headers, requestParameters, uploadFileName, demographicNo);
				//make sure docId is proper. Else it will throw number format exception.
				Long docId = Long.parseLong(uploadDocId);
				if(docId != -1){
					nameValueList.add(new BasicNameValuePair("attachment[" + i + "]", uploadDocId));
					i++;	
				}
			}
		}

		//create the url
		URIBuilder builder = new URIBuilder();
		builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
		.setPath(ConfigData._remote_App_Context + ConfigData._add_Form_Remote_URL_For_Remote);
		String uriString = builder.build().toString();
		uriString = uriString + "?" + queryString;
		log.debug(uriString);
		
		//fix for demographicNo. New client creates a new demographicNo.
		builder = new URIBuilder(uriString);
		builder.setParameter(PARAM_NAME_EFMDEMOGRAPHIC_NO, demographicNo);
		uriString = builder.build().toString();
		
		HttpPost httpost = new HttpPost(uriString);
		httpost.setEntity(new UrlEncodedFormEntity(nameValueList, Consts.UTF_8));
		httpost.setHeaders(headers);
		HttpResponse response = null;
		try {
			System.out.println("Uploading eform to server...");
			response = httpClient.execute(httpost);
			HttpEntity entity = response.getEntity();
			System.out.println("Uploading eform to server..." + response.getStatusLine());
			htmlResponseText = EntityUtils.toString(entity, Consts.UTF_8);
			System.out.println("Uploaded eform to server.");
		} finally {
			httpost.releaseConnection();
		}
		
		if (200 == response.getStatusLine().getStatusCode()){
			// send email to the wl operator
			builder = new URIBuilder();
			builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
			.setPath(ConfigData._remote_App_Context + ConfigData._trigger_email_url)
			.setParameter(PARAM_NAME_FID,ConfigData._param_fid)
			.setParameter(PARAM_NAME_DEMOGRAPHIC_NO,ConfigData._param_demographic_no)
			.setParameter(PARAM_NAME_APPOINTMENT,ConfigData._param_appointment)
			.setParameter(ConfigData._client_refferel_param_program_id, ConfigData._client_refferel_param_program_id_value);
			uriString = builder.build().toString();
			httpost = new HttpPost(uriString);
			System.out.println("Trigger email notification!");
			try{
				HttpResponse rsp = httpClient.execute(httpost);
				if (200 != rsp.getStatusLine().getStatusCode()){
					log.debug("Failed to trigger email!\n"+rsp.getStatusLine());
				}
			}catch(Exception e){
				log.debug(e.toString());
			} finally {
				httpost.releaseConnection();
			}
		}

		return refferClientToService(httpClient, headers, demographicNo);
		
		//return htmlResponseText;
	}
	
	public String uploadFormAttachmentData(HttpClient httpClient, Header[] headers,
			Map<String, String[]> requestParameters, String uploadFileName, String demographicNo) throws ParseException,
			ClientProtocolException, IOException, URISyntaxException {
		log.debug("In uploadFormAttachmentData");
		HttpPost httpost = null;
		try {
			URIBuilder builder = new URIBuilder();
			builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
			.setPath(ConfigData._remote_App_Context + ConfigData._upload_form_remote_url)
			.setParameter(PARAM_NAME_EFMDEMOGRAPHIC_NO, demographicNo);
			URI uri = builder.build();
			log.debug(uri.toString());
			
			httpost = new HttpPost(uri);
			httpost.setHeaders(headers);

			List<org.apache.http.NameValuePair> nvps = new ArrayList<org.apache.http.NameValuePair>();
			nvps.add(new BasicNameValuePair(ProxyAppConstants.UPLOAD_FILE_FIELD, uploadFileName));
			
//			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//			entity.addPart("attachedFile", new FileBody(uploadFile));
//			httpost.setEntity(entity);
			
			httpost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
			String responseText = EntityUtils.toString(httpClient.execute(httpost).getEntity(), Consts.UTF_8);
			System.out.println("Uploaded attachment.");
			log.info(responseText);
			return responseText;
		} finally {
			httpost.releaseConnection();
		}
	}
	
	private URI getRemoteURI(String path) throws URISyntaxException {
		URIBuilder builder = new URIBuilder();
		builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port).setPath(ConfigData._remote_App_Context + path);
		return builder.build();
	}
	
	public String refferClientToService(HttpClient httpClient, Header[] headers, String demographic_id) 
			throws ClientProtocolException, IOException, URISyntaxException {
		log.debug("In refferClientToService");
		
		//associateSelectedFacilityWithClient(httpClient, headers);
		
		String htmlResponseText = null;
		String htmlResponseMessage = "";
		List<NameValuePair> nameValueList = new ArrayList<NameValuePair>();

		for (Entry<String, String> entry : getRefferelRequestParameters(demographic_id).entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if(key != null && !"".equals(key) && value != null){
				nameValueList.add(new BasicNameValuePair(key, value));
			}
		}

		//create the url
		URIBuilder builder = new URIBuilder();
		builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
		.setPath(ConfigData._remote_App_Context + ConfigData._client_refferel_remote_url);
		String uriString = builder.build().toString();
		//uriString = uriString + "?" + queryString;
		log.debug(uriString);
		
		HttpPost httpost = new HttpPost(uriString);
		httpost.setEntity(new UrlEncodedFormEntity(nameValueList, Consts.UTF_8));
		httpost.setHeaders(headers);

		try {
			System.out.println("reffering Client To Service...");
			HttpResponse response = httpClient.execute(httpost);
			HttpEntity entity = response.getEntity();
			System.out.println("reffering Client To Service..." + response.getStatusLine());
			
			htmlResponseText = EntityUtils.toString(entity, Consts.UTF_8);
			System.out.println("Client reffered to service id: " + "" + ", successfully");
			
			if(ConfigData.HTTP_RESPONSE_200_OK.equalsIgnoreCase(response.getStatusLine().toString()))
			{
				htmlResponseMessage = EFormProxyUtility.getHTMLMessage(ConfigData._client_Refferel_Success_Message);
			}
			else
			{
				htmlResponseMessage = EFormProxyUtility.getHTMLMessage(ConfigData._client_Refferel_Failure_Message);
			}
		} finally {
			httpost.releaseConnection();
		}

		//return htmlResponseText;
		return htmlResponseMessage;
	}
	
//	private void associateSelectedFacilityWithClient(HttpClient httpClient, Header[] headers)
//			throws ClientProtocolException, IOException, URISyntaxException
//	{
//		log.debug("In associateSelectedFacilityWithClient");
//		
//		String htmlResponseText = null;
//		List<NameValuePair> nameValueList = new ArrayList<NameValuePair>();
//
//		for (Entry<String, String> entry : getRefferelFacilityRequestParameters().entrySet()) {
//			String key = entry.getKey();
//			String value = entry.getValue();
//			if(key != null && !"".equals(key) && value != null){
//				nameValueList.add(new BasicNameValuePair(key, value));
//			}
//		}
//
//		//create the url
//		URIBuilder builder = new URIBuilder();
//		builder.setScheme(ConfigData._remote_Host_Scheme).setHost(ConfigData._remote_Host_IP).setPort(ConfigData._remote_Host_Port)
//		.setPath(ConfigData._remote_App_Context + ConfigData._client_refferel_facility_remote_url);
//		String uriString = builder.build().toString();
//		//uriString = uriString + "?" + queryString;
//		log.debug(uriString);
//		
//		HttpPost httpost = new HttpPost(uriString);
//		httpost.setEntity(new UrlEncodedFormEntity(nameValueList, Consts.UTF_8));
//		httpost.setHeaders(headers);
//
//		try {
//			System.out.println("associating Selected Facility With Client...");
//			htmlResponseText = EntityUtils.toString(httpClient.execute(httpost).getEntity(), Consts.UTF_8);
//			System.out.println("Selected facility id: " + ConfigData._client_refferel_facility_param2_value + ", associated with the client successfully");
//		} finally {
//			httpost.releaseConnection();
//		}
//
//		System.out.println("ResponseContent: " + htmlResponseText);
//	}
	
	private Map<String, String> getRefferelRequestParameters(String demographic_id)
	{
		Map<String, String> requestParameters = new HashMap<String, String>();
		requestParameters.put(ConfigData._client_refferel_param_demographic_no, demographic_id);
		requestParameters.put(ConfigData._client_refferel_param_method, ConfigData._client_refferel_param_method_value);
		requestParameters.put(ConfigData._client_refferel_param_program_id, ConfigData._client_refferel_param_program_id_value);
		requestParameters.put(ConfigData._client_refferel_param_reason, ConfigData._client_refferel_param_reason_value);
		requestParameters.put(ConfigData._client_refferel_param_view_tab, ConfigData._client_refferel_param_view_tab_value);
		return requestParameters;
	}
	
//	private Map<String, String> getRefferelFacilityRequestParameters()
//	{
//		Map<String, String> requestParameters = new HashMap<String, String>();
//		requestParameters.put(ConfigData._client_refferel_facility_param1, ConfigData._client_refferel_facility_param1_value);
//		requestParameters.put(ConfigData._client_refferel_facility_param2, ConfigData._client_refferel_facility_param2_value);
//		
//		return requestParameters;
//	}
}




